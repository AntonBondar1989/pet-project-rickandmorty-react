import React, { useEffect, useState } from "react";
import axios from "axios";
import Loader from "../loader/Loader";
import { getPageCount, getPagesArray } from "../../utils/pages";
import Pagination from "../pagination/Pagination";
import MyInput from "../input/MyInput";
import SelectButton from "../select-button/SelectButton";
import HeroesItem from "../heroes-item/HeroesItem";

import cl from "./HeroesPage.module.css"

const HeroesPage = () => {

   const [heroes, setHeroes] = useState([])
   const [onLoading, setOnLoading] = useState(true);
   const [totalPages, setTotalPages] = useState(0)
   const [limit, setLimit] = useState(20)
   const [page, setPage] = useState(1)
   const [nameHero, setNameHero] = useState('')
   const [statusHero, setStatusHero] = useState('')

   // Масив сторінок
   let pagesArray = getPagesArray(totalPages)

   // ЗАПИТ НА СЕРВЕР
   async function getAllHeroes() {
      const response = await axios.get(`https://rickandmortyapi.com/api/character?name=${nameHero}&status=${statusHero}&page=${page}`)
      setHeroes(response.data.results)
      const totalCount = response.data.info.count;
      setTotalPages(getPageCount(totalCount, limit))
      setOnLoading(false)
   }

   useEffect(() => {
      getAllHeroes()
   }, [page, nameHero, statusHero])

   // Видяляємо героя
   const removeHeroes = (el) => {
      setHeroes(heroes.filter(p => p.id !== el.id))
   }

   return (
      <>
         <hr />
         <Pagination setPage={setPage} pagesArray={pagesArray} page={page} />
         <hr />
         <div className={cl.search_input}>
            <MyInput
               value={nameHero}
               onChange={e => setNameHero(e.target.value)}
               placeholder="Search name..." />
            <div className={cl.select_status_zone}>
               <span className={cl.select_status}>Select status:</span>
               <SelectButton onClick={() => setStatusHero('alive')}>alive</SelectButton>
               <SelectButton onClick={() => setStatusHero('dead')}>dead</SelectButton>
               <SelectButton onClick={() => setStatusHero('')}>reset</SelectButton>
            </div>
         </div>
         <div >
            {onLoading ?
               <div>
                  <Loader />
               </div> :
               <HeroesItem heroes={heroes} removeHeroes={removeHeroes} />
            }
         </div>
         <hr />
         <Pagination setPage={setPage} pagesArray={pagesArray} page={page} />
         <hr />
      </>
   )
}

export default HeroesPage;