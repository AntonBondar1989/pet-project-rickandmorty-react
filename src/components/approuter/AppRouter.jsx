import React from "react";
import { Route, Routes } from "react-router-dom";
import Earth from "../../pages/Earth";
import EarthIdPage from "../../pages/EarthIdPage";
import Heroes from "../../pages/Heroes";
import HeroesIdPage from "../../pages/HeroesIdPage";
import Main from "../../pages/Main";

const AppRouter = () => {
   return (
      <Routes>
         <Route path="/main" element={<Main />} />
         <Route path="/" element={<Main />} />
         <Route exact path="/heroes" element={<Heroes />} />
         <Route exact path="/earth" element={<Earth />} />
         <Route exact path="/earth/:id" element={<EarthIdPage />} />
         <Route exact path="/heroes/:id" element={<HeroesIdPage />} />
      </Routes>
   )
}

export default AppRouter;