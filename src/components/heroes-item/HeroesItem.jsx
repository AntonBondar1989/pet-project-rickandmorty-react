import React from "react";
import { useNavigate } from "react-router-dom";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import MyButton from "../button/MyButton";

import "../../styles/index.css"
import cl from "../heroespage/HeroesPage.module.css"

const HeroesItem = (props) => {

   const router = useNavigate()//Перехід

   return (
      <>
         <TransitionGroup className={cl.heroes_page}>
            {props.heroes.map(el => (
               <CSSTransition
                  key={el.id}
                  timeout={500}
                  classNames="item"
               >
                  <div key={el.id} className={cl.box_hero}>
                     <div>
                        <img className={cl.box_image} src={el.image} alt="picture" />
                     </div>
                     <div className={cl.box_text}>
                        <h3 style={{ color: "blue" }}>{el.name}</h3>
                        <div>
                           <span style={{ color: "goldenrod" }}>status:</span>
                           <span className={el.status === "Dead" ? cl.red : cl.green}>{el.status}</span>
                        </div>
                        <MyButton onClick={() => router(`/heroes/${el.id}`)}>Open</MyButton>
                        <MyButton onClick={() => props.removeHeroes(el)}>Delete</MyButton>
                     </div>
                  </div>
               </CSSTransition>
            ))
            }
         </TransitionGroup>
      </>
   )
}

export default HeroesItem;