import React from "react";

import cl from "./SelectButton.module.css"

const SelectButton = ({children, ...props}) => {
   return (
      <button {...props} className={cl.select_btn}>
      {children}
   </button>
   )
}

export default SelectButton;