import React, { useEffect, useState } from "react";
import { getPageCount, getPagesArray } from "../../utils/pages";
import axios from "axios";
import Pagination from "../pagination/Pagination";
import MyInput from "../input/MyInput";
import PlanetItem from "../planetitem/PlanetItem";



const PlanetPage = () => {

   const [planet, setPlanet] = useState([])
   const [totalPages, setTotalPages] = useState(0)
   const [limit, setLimit] = useState(20)
   const [page, setPage] = useState(1)
   const [namePlanet, setNamePlanet] = useState('')

   // Масив сторінок
   let pagesArray = getPagesArray(totalPages)

   // ЗАПИТ НА СЕРВЕР
   async function getAllPlanet() {
      const response = await axios.get(`https://rickandmortyapi.com/api/location?name=${namePlanet}&page=${page}`)
      setPlanet(response.data.results)
      const totalCount = response.data.info.count;
      setTotalPages(getPageCount(totalCount, limit))
   }

   useEffect(() => {
      getAllPlanet()
   }, [page, namePlanet])

   return (
      <>
         <hr />
         <Pagination setPage={setPage} pagesArray={pagesArray} page={page} />
         <hr />
         <div style={{ textAlign: "center" }}>
            <MyInput
               value={namePlanet}
               onChange={e => setNamePlanet(e.target.value)}
               placeholder="Search planet..." />
         </div>
         <PlanetItem planet={planet} />
         <hr />
         <Pagination setPage={setPage} pagesArray={pagesArray} page={page} />
         <hr />
      </>

   )
}

export default PlanetPage;