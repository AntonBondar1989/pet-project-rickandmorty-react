import React from "react";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { useNavigate } from "react-router-dom";

import planet1 from "../../images/planet1.gif"
import planet2 from "../../images/planet2.gif"
import planet3 from "../../images/planet3.gif"
import planet4 from "../../images/planet4.gif"
import planet5 from "../../images/planet5.gif"
import planet6 from "../../images/planet6.gif"
import planet7 from "../../images/planet7.gif"
import planet8 from "../../images/planet8.gif"

import "../../styles/index.css"
import cl from "./PlanetItem.module.css"

const PlanetItem = (props) => {

   const allPlanets = [planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8]
   const rand = Math.floor(Math.random() * allPlanets.length);

   const router = useNavigate()

   return (
      <TransitionGroup className={cl.planet_item}>
         {props.planet.map(el => (
            <CSSTransition
               key={el.id}
               timeout={500}
               classNames="item"
            >
               <div onClick={() => router(`/earth/${el.id}`)} className={cl.planet_box}>
                  <div>
                     <img src={allPlanets[rand]} alt="img" />
                  </div>
                  <h2 className={cl.name_planet}>{el.name}</h2>
               </div>
            </CSSTransition>
         ))}

      </TransitionGroup>
   )
}

export default PlanetItem;