import React from "react";
import loader from '../../images/loader3.svg'

const Loader = () => {
   return (
      <div>
         <img src={loader} alt="loader" />
      </div>
   )
}

export default Loader;
