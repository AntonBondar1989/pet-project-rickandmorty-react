import React from "react";
import Logo from "../../images/logo.png"
import { Link } from "react-router-dom";

import cl from "./Navbar.module.css"

const Navbar = () => {
   return (
      <div className={cl.navbar}>
         <div style={{ marginLeft: 20 }}>
            <Link to="./main"><img className={cl.navbar_logo} src={Logo} alt="Logo" /></Link>
         </div>
         <div>
            <details >
               <summary className={cl.summary}>Navigation</summary>
               <ul className={cl.navbar_list}>
                  <Link to='./main'><li>Main</li></Link>
                  <Link to='./heroes'><li>Heroes</li></Link>
                  <Link to='./earth'><li>Earth</li></Link>
               </ul>
            </details>
         </div>
      </div>
   )
}

export default Navbar;