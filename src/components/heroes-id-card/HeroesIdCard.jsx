import React from "react";
import cl from "./HeroesIdCard.module.css";

const HeroesIdCard = (props) => {

   // Страхуюся щоб не отримати undefined (вложені обьекти)
   const nameEarth = props.hero && props.hero.location && props.hero.location.name;
  
   return (
      <div className={cl.heroes_id_card}>
         <div>
            <img className={cl.img} src={props.hero.image} alt="img" />
         </div>
         <div><h1 style={{ color: 'blue' }}>{props.hero.name}</h1></div>
         <div><span style={{ color: "goldenrod" }}>status: </span><span className={props.hero.status === 'Dead' ? cl.red : cl.green}>{props.hero.status}</span></div>
         <div><span style={{ color: "goldenrod" }}>species: </span>{props.hero.species}</div>
         <div><span style={{ color: "goldenrod" }}>gender: </span>{props.hero.gender}</div>
         <div><span style={{ color: "goldenrod" }}>location: </span>{nameEarth}</div>
         <div><span style={{ color: "goldenrod" }}>created: </span>{props.hero.created}</div>
      </div>
   )
}

export default HeroesIdCard;