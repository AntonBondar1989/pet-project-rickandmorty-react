import React from "react";

import cl from "./Pagination.module.css";

const Pagination = (props) => {
   return (
      <div className={cl.pagination}>
         {props.pagesArray.map(p =>
            <button onClick={() => props.setPage(p)} key={p} className={props.page === p ? cl.page_current : cl.page}>
               {p}
            </button>
         )}
      </div>
   )
}

export default Pagination;