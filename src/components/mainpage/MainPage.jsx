import React from "react";
import cl from "./MainPage.module.css"
import rm1 from "../../images/rm1.png"
import rm2 from "../../images/rm2.png"
import rm3 from "../../images/rm3.png"

const MainPage = () => {
   return (
      <>
         <div className={cl.main_page_block}>
            <div><img className={cl.img} src={rm1} alt="img" /></div>
            <div><img className={cl.img} src={rm2} alt="img" /></div>
            <div><img className={cl.img} src={rm3} alt="img" /></div>
         </div>
      </>
   )
}

export default MainPage;