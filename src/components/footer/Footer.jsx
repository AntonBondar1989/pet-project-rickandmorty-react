import React from "react";
import cl from "./Footer.module.css"

const Footer = () => {
   return (
      <footer className={cl.footer}>
         &copy; Copyright 2022 All Right Reserved
      </footer>
   )
}

export default Footer;