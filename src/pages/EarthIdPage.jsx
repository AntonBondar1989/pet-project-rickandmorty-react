import axios from "axios";
import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";

const EarthIdPage = () => {

   // Для отримання айдішника
   const params = useParams()

   const [numberPlanet, setNumberPlaner] = useState(Number(params.id))
   const [planet, setPlanet] = useState({})
  
   // Для навігації
   const router = useNavigate()

   async function getHero() {
      const response = await axios.get(`https://rickandmortyapi.com/api/location/${numberPlanet}`)
      setPlanet(response.data)
   }
 
   useEffect(() => {
      getHero()
   }, [numberPlanet])

   return (
      <div className="page">
         <h1>Page ID = {params.id}</h1>
      </div>

   )
}

export default EarthIdPage;