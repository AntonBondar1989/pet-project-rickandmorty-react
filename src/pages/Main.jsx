import React from "react";
import MainPage from "../components/mainpage/MainPage";

const Main = () => {
   return (
      <div className="page">
        <MainPage/>
      </div>
   )
}

export default Main;