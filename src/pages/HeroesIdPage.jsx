import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import MyButton from "../components/button/MyButton";
import HeroesIdCard from "../components/heroes-id-card/HeroesIdCard";

const HeroesIdPage = () => {
   // Для отримання айдішника
   const params = useParams()

   const [hero, setHero] = useState({})
   const [numberHero, setNumberHero] = useState(Number(params.id))
   // Для навігації
   const router = useNavigate()

   async function getHero() {
      const response = await axios.get(`https://rickandmortyapi.com/api/character/${numberHero}`)
      setHero(response.data)
   }

   useEffect(() => {
      getHero()
   }, [numberHero])

   return (
      <div className="page">
         <hr />
         <div style={{ textAlign: 'center' }}>
            <div>
               <MyButton onClick={() => router(`/heroes`)}>Back to all heroes</MyButton>
            </div>
            <div>
               <MyButton onClick={() => setNumberHero(numberHero - 1)}> Preview Hero</MyButton>
               <MyButton onClick={() => setNumberHero(numberHero + 1)}>Next Hero</MyButton>
            </div>
         </div>
         <HeroesIdCard hero={hero} />
      </div>
   )
}

export default HeroesIdPage;