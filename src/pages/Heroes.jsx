import React from "react";
import HeroesPage from "../components/heroespage/HeroesPage";

const Heroes = () => {
   return (
      <div className="page">
         <HeroesPage/>
      </div>
   )
}

export default Heroes;