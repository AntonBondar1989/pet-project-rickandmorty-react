import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import AppRouter from './components/approuter/AppRouter';
import Footer from './components/footer/Footer';
import Navbar from './components/navbar/Navbar';
import './styles/App.css';

function App() {
  return (
    <div className='app'>
      <BrowserRouter>
        <Navbar />
        <AppRouter />
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
